sap.ui.define([
    "sap/ui/core/mvc/Controller"
],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller) {
        "use strict";

        return Controller.extend("zui5002.controller.View1", {
            onInit: function () {

            },
            onNav() {
                var xnavservice = sap.ushell && sap.ushell.Container && sap.ushell.Container.getService("CrossApplicationNavigation");
                var href = (xnavservice && xnavservice.hrefForExternal({
                    target: { semanticObject: "FioriApp", action: "display" },
                    params: { "parameter": this.getView().byId("Input").getValue() }
                })) || "";

                var finalUrl = window.location.href.split("#")[0] + href;
                sap.m.URLHelper.redirect(finalUrl, true);
            }
        });
    });
