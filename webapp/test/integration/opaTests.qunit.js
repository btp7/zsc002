/* global QUnit */

sap.ui.require(["zui5002/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
